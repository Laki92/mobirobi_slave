#include <Wire.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "I2Cdev.h"

#define enableSerwo true
int pinMotor1a = 8; // d8
int pinMotor1b = 7; // d7 .    // kreci
int pinMotor1s = 5; // d5 pwm szybciej / wolniej
int pinMotor2a = 10; // d10
int pinMotor2b = 9; // d9 .   /// kreci
int pinMotor2s = 6; // d6 pwm

unsigned long wireTimer = 0;
int maxCommDelay = 1000; // maximum time between commands [ms]

uint16_t packetSize = 0;
MPU6050 accelerometer(0x69);

void setup() {
  Wire.begin(10);
  Serial.begin(38400);
  while (!Serial);
  Serial.println("Slave is ready with on 10");

  analogWrite(pinMotor1s, 0);
  pinMode(pinMotor1a, OUTPUT);
  pinMode(pinMotor1b, OUTPUT);

  analogWrite(pinMotor2s, 0);
  pinMode(pinMotor2a, OUTPUT);
  pinMode(pinMotor2b, OUTPUT);

  initGyro(accelerometer, packetSize);

  Wire.onReceive(receiveEvent);
}

int mNumber = -1;
char mLastline[50] = {};

void loop() {
  checkCommunication();

  if(mNumber != -1){
    commands(mNumber, mLastline);
    mNumber = -1;
  }

  if (Serial.available() > 0)
  {
    char lastline[50];
    int number = readSerial(lastline);
    //Serial.read();
    commands(number, lastline);
    String msg = String(number) + String(",") + String(lastline);
    Serial.println(msg);
    wireTimer = millis();
  }
  delay(5);
}

void receiveEvent(int howMany) {
  while (0 < Wire.available()) { // loop through all but the last
    mNumber = readWire(mLastline);
    String msg = String(mNumber) + String(",") + String(mLastline);
    Serial.println(msg);
    wireTimer = millis();
  }
}

void checkCommunication()
{
  if (millis() - wireTimer > maxCommDelay)
  {
    serwo1(0, LOW, LOW);
    serwo2(0, LOW, LOW);
  }
}

void commands(int number, char lastline[50]) {
  if (strcmp(lastline, "q") == 0) {
    serwo1(number, LOW, HIGH);
  }

  if (strcmp(lastline, "w") == 0) {
    serwo1(number, HIGH, LOW);
  }

  if (strcmp(lastline, "e") == 0) {
    serwo1(number, LOW, LOW);
  }

  if (strcmp(lastline, "a") == 0) {
    serwo2(number, HIGH, LOW);
  }

  if (strcmp(lastline, "s") == 0) {
    serwo2(number, LOW, HIGH);
  }

  if (strcmp(lastline, "d") == 0) {
    serwo2(number, LOW, LOW);
  }

  if (strcmp(lastline, "h") == 0) {
   wireWrite(7, getGyro("GYRO2#", accelerometer));
  }

  if(strcmp(lastline, "l") == 0){
    calibrateGyro(accelerometer);
  }

}
